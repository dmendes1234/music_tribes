from django.apps import AppConfig


class MusictribesConfig(AppConfig):
    name = 'musicTribes'
