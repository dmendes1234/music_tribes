from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Tribe, Playlist, Message, Track, Comment, Profile


# Create your forms here.

class NewUserForm(UserCreationForm):
	email = forms.EmailField(required=True)

	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")

	def save(self, commit=True):
		user = super(NewUserForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'username',
            'email'
        )

class EditProfileImage(forms.ModelForm):
	class Meta:
		model = Profile
		fields = (
			'profile_pic',
		)


class TribeForm(ModelForm):
    class Meta:
        model = Tribe
        fields = ['name', 'genre', 'image']



class PlaylistForm(ModelForm):
	class Meta:
		model = Playlist
		fields = ['name', 'description']


class TrackForm(ModelForm):
	class Meta:
		model = Track
		fields = ['title', 'artist', 'youtube_url', 'duration']


class MessageForm(ModelForm):
	class Meta:
		model = Message
		fields = ['text']


class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ['text']