from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    profile_pic = models.ImageField(default="blank-profile-picture.png", null=True, blank=True)


class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Tribe(models.Model):

    GENRE_CHOICES = [
        ('pop', 'pop'),
        ('rock', 'rock'),
        ('folk', 'folk'),
        ('jazz', 'jazz'),
        ('hip-hop', 'hip-hop'),
        ('electronic', 'electronic'),
        ('heavy metal', 'heavy metal'),
        ('punk', 'punk'),
        ('country', 'country'),
        ('blues', 'blues'),
        ('house', 'house'),
    ]

    name = models.CharField(max_length=200, unique=True)
    user = models.ForeignKey(User, related_name="user+", on_delete=models.CASCADE)
    genre = models.CharField(max_length=50, choices=GENRE_CHOICES, default='heavy metal',)
    image = models.ImageField(default='', upload_to='tribe_pics')
    member = models.ManyToManyField(User, related_name="member+")

    def __str__(self):
        return self.name


class Playlist(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True) 
    description = models.TextField(max_length=1000)

    def __str__(self):
        return self.name


class Track(TimeStamped):
    title = models.CharField(max_length=200)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    artist = models.CharField(max_length=200)
    youtube_url = models.CharField(max_length=500)
    duration = models.IntegerField(default=0)
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)


    def upvote_score(self):
        upvotes = self.vote_set.filter(upvote=True).count()
        return upvotes

    def downvote_score(self):
        downvotes = self.vote_set.filter(upvote=False).count()
        return downvotes

    def vote_score(self):
        upvotes = self.vote_set.filter(upvote=True).count()
        downvotes = self.vote_set.filter(upvote=False).count()
        return upvotes - downvotes
    
    def vote_by(self, user):
        if user.is_authenticated:
            return Vote.objects.filter(user=user, track=self).first()
        else:
            return None

    def __str__(self):
        return self.title




class Vote(TimeStamped):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    upvote = models.BooleanField(null=False, default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='user_vote', fields=['track', 'user'])
        ]

    def __str__(self):
        return f"{self.user.username} voted on {self.track.title}"
    
    def downvote(self):
        return not self.upvote



class Message(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    image = models.ImageField(default="blank-profile-picture.png", null=True, blank=True)

    def __str__(self):
        return self.text


class Comment(TimeStamped):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    image = models.ImageField(default="blank-profile-picture.png", null=True, blank=True)

    def __str__(self):
        return self.text

