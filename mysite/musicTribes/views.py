from django.shortcuts import  render, redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .forms import NewUserForm, TribeForm, PlaylistForm, MessageForm, TrackForm, CommentForm
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth import login, authenticate
from django.contrib import messages #import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from musicTribes.forms import EditProfileForm, EditProfileImage
from .models import Tribe, Playlist, Message, Track, Vote, Comment, Profile
# Create your views here.

def index(request):
    if request.user.is_authenticated:
        user_tribes = Tribe.objects.all().filter(user = request.user)
        member_tribes = Tribe.objects.all().filter(member = request.user)
        free_tribes = Tribe.objects.all().exclude(user = request.user).exclude(member = request.user)
        context = {
            "user_tribes":user_tribes, 
            "free_tribes":free_tribes,
            "member_tribes":member_tribes
        }
        return render(request, "musicTribes/index.html", context)
    else:
        all_tribes=Tribe.objects.all()
    return render(request, "musicTribes/index.html", {"all_tribes":all_tribes})


def register(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			Profile.objects.create(
				user=user,
				)
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect('index')
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm
	return render (request=request, template_name="musicTribes/register.html", context={"register_form":form})

def login_(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}")
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "musicTribes/login.html",
                    context={"form":form})

def logout_(request):
	logout(request)
	messages.info(request, "Logged out successfully!")
	return redirect('index')

@login_required
def edit_user(request):
    profile = request.user.profile
    form = EditProfileForm(instance=request.user)
    form_image = EditProfileImage(instance=profile)

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        form_image = EditProfileImage(request.POST, request.FILES, instance=profile)
        if form.is_valid() and form_image.is_valid():
            form.save()
            form_image.save()
            messages.info(request, "Profile data successfully updated!")
            return redirect(reverse('edit_user'))
        else:
            messages.error(request, "Invalid information! User with that username or email already exists!")
            return redirect(reverse('edit_user'))

    context = {
        'form': form,
        'form_image': form_image 
    }
    return render(request, 'musicTribes/edit_user.html', context)

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.info(request, "Password successfully changed!")
            return redirect(reverse('change_password'))
        else:
            messages.error(request, "Invalid information. Try again!")
            return redirect(reverse('change_password'))
    else:
        form = PasswordChangeForm(user=request.user)

        args = {'form': form}
        return render(request, 'musicTribes/change_password.html', args)


@login_required
def create_tribe(request):
    form = TribeForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        tribe = form.save(commit=False)
        tribe.user = request.user
        tribe.image = request.FILES['image']
        file_type = tribe.image.url.split('.')[-1]
        file_type = file_type.lower()
        saved_tribe = tribe.save()
        
        return redirect(reverse('index'))
    context = {
        "form": form
    }
    return render(request, 'musicTribes/create_tribe.html', context)


@login_required
def join_tribe(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user

    if request.method == 'POST':
        tribe.member.add(user)
        tribe.save()
        messages.info(request, 'You have joined to ' + tribe.name)
        return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe_id}))
    
    return render(request, 'musicTribes/index.html')
    


def tribe_details(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = Playlist.objects.all().filter(tribe=tribe_id)
    members = tribe.member.all()
    form = MessageForm(request.POST)
    messages_ = Message.objects.all().filter(tribe=tribe_id)
    context = {
        'tribe':tribe,
        'playlists':playlists,
        'members':members,
        'form':form,
        'messages_':messages_
    }
    return render(request, 'musicTribes/tribe_details.html', context)


@login_required
def create_playlist(request, tribe_id):
    form =PlaylistForm(request.POST or None, request.FILES or None)
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    if form.is_valid():
        playlist = form.save(commit=False)
        playlist.user = request.user
        playlist.tribe = tribe
        saved_playlist = playlist.save()
        return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))
    context = {
        "form": form
    }
    return render(request, 'musicTribes/create_playlist.html', context)


@login_required
def delete_playlist(request, tribe_id, playlist_id):
    playlist = get_object_or_404(Playlist, pk=playlist_id)
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    if request.method == 'POST' and request.user.is_authenticated:
        messages.info(request, 'The playlist ' + playlist.name + ' is deleted')
        playlist.delete()

    return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))


@login_required
def edit_playlist(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(Playlist, pk=playlist_id)
    if request.method == 'POST':
        form = PlaylistForm(request.POST, instance=playlist)
        if form.is_valid():
            form.save()
            messages.info(request, "Playlist data successfully updated!")
            return redirect(reverse('edit_playlist', kwargs={'tribe_id':tribe.id, 'playlist_id':playlist.id}))

    form = PlaylistForm(instance=playlist)
    context = {'form': form, 'playlist':playlist, 'tribe':tribe}
    return render(request, 'musicTribes/edit_playlist.html', context)


@login_required
def kick_member(request, tribe_id, member_name):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = Playlist.objects.all().filter(tribe=tribe_id)
    members = tribe.member.all()
    form = MessageForm(request.POST)
    messages_ = Message.objects.all().filter(tribe=tribe_id)
    user_to_remove = tribe.member.get(username = member_name)
    if request.method == 'POST' and request.user.is_authenticated and tribe.user == request.user:
        messages.info(request, 'You kicked ' + member_name + ' from ' + tribe.name)
        tribe.member.remove(user_to_remove)
        tribe.save()
        return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))
    
    context = {
        'tribe':tribe,
        'playlists':playlists,
        'members':members,
        'form':form,
        'messages_': messages_,
    }
    return render(request, 'musicTribes/tribe_details.html', context)


@login_required
def leave_tribe(request, tribe_id, user_name):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = Playlist.objects.all().filter(tribe=tribe_id)
    members = tribe.member.all()
    form = MessageForm(request.POST)
    messages_ = Message.objects.all().filter(tribe=tribe_id)
    user_to_remove = tribe.member.get(username = user_name)
    
    if request.method == 'POST' and request.user.is_authenticated:
        messages.info(request, 'You left ' + tribe.name)
        tribe.member.remove(user_to_remove)
        tribe.save()
        return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))
    
    context = {
        'tribe':tribe,
        'playlists':playlists,
        'members':members,
        'form':form,
        'messages_': messages_,
    }
    return render(request, 'musicTribes/tribe_details.html', context)    


@login_required
def send_message(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = Playlist.objects.all().filter(tribe=tribe_id)
    members = tribe.member.all()
    form = MessageForm(request.POST)
    
    if request.method == 'POST':
        if form.is_valid():
            message = form.save(commit=False)
            message.user = request.user
            message.tribe = tribe
            message.image = request.user.profile.profile_pic
            message.save()
            return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))

    context = {
        'tribe':tribe,
        'playlists':playlists,
        'members':members,
        'messages_': Message.objects.all().filter(tribe=tribe_id),
        'form':form
    }
    return render(request, 'musicTribes/tribe_details.html', context) 

        

@login_required
def delete_message(request, tribe_id, message_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    message = Message.objects.all().filter(pk = message_id)
    user = request.user

    if request.method == 'POST' and user.is_authenticated and tribe.user == user:
        message.delete()
    
    return redirect(reverse('tribe_details', kwargs={'tribe_id':tribe.id}))


def playlist_details(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(Playlist, pk=playlist_id)
    form = CommentForm

    track_set = Track.objects.all().filter(playlist=playlist)
    tracks = [] 

    for track in track_set:
        track_item = {}
        track_item['track'] = track
        track_item['comments'] = track.comment_set.all()
        tracks.append(track_item)
    
    context = {
        'tribe':tribe,
        'playlist':playlist,
        'tracks':tracks,
        'form':form
        }

    return render(request, 'musicTribes/playlist_details.html', context)


@login_required
def add_track(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(Playlist, pk=playlist_id)
    form = TrackForm(request.POST or None, request.FILES or None)
    user = request.user

    if form.is_valid():
        track = form.save(commit=False)
        track.user = user
        track.playlist = playlist
        saved_track = track.save()
        return redirect(reverse('playlist_details', kwargs={'tribe_id':tribe_id, 'playlist_id':playlist_id}))

    context = {
        'form':form
    }    
    return render(request, 'musicTribes/add_track.html', context)


@login_required
def delete_track(request, tribe_id, playlist_id, track_id):
    track = get_object_or_404(Track, pk=track_id)
    if request.method == 'POST' and request.user.is_authenticated or request.user.is_superuser or track.user == request.user:
        track.delete()

    return redirect(reverse('playlist_details', kwargs={'tribe_id':tribe_id, 'playlist_id':playlist_id}))


@login_required
def vote(request, track_id, upvote):
    track = get_object_or_404(Track, pk=track_id)
    vote = Vote.objects.filter(user=request.user, track=track).first()
    if vote:
        if vote.upvote == upvote:
            vote.delete()
            return None
        else:
            vote.upvote = upvote
    else:
        vote = Vote(user=request.user, track=track, upvote=upvote)
    try:
        vote.full_clean()
        vote.save()
    except Exception as e:
        print(e)
        return None
    else:
        return vote


@login_required
def upvote(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user
    if request.method == 'POST' and request.user.is_authenticated or tribe.user == user or user in tribe.member.all():
        vote(request, track_id, True)
    return HttpResponseRedirect(reverse('playlist_details', args=(tribe_id, playlist_id,)))


@login_required
def downvote(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user
    if request.method == 'POST' and request.user.is_authenticated or tribe.user == user or user in tribe.member.all():
        vote(request, track_id, False)
    return HttpResponseRedirect(reverse('playlist_details', args=(tribe_id, playlist_id,)))


@login_required
def comment(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(Playlist, pk=playlist_id)
    tracks = Track.objects.all().filter(playlist=playlist)
    form = CommentForm(request.POST)
    track = get_object_or_404(Track, pk=track_id)
    comments = Comment.objects.all().filter(track = track)
    user = request.user
    
    if request.method == 'POST' and user.is_authenticated or tribe.user == user or user in tribe.member.all(): 
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.track = track
            comment.image = request.user.profile.profile_pic
            saved_comment = comment.save()
            return redirect(reverse('playlist_details', kwargs={'tribe_id':tribe.id, 'playlist_id':playlist.id}))

    context = {
        'tribe':tribe,
        'playlist':playlist,
        'tracks':tracks,
        'comments':comments,
        'form':form
    }
    return render(request, 'musicTribes/playlist_details.html', context)


@login_required
def delete_comment(request, tribe_id, playlist_id, comment_id):
    comment = get_object_or_404(Comment, pk = comment_id)
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user

    if request.method == 'POST' and user.is_authenticated and user.is_superuser or tribe.user == user:
        comment.delete()

    return redirect(reverse('playlist_details', kwargs={'tribe_id':tribe_id, 'playlist_id':playlist_id}))