from django.contrib import admin
from .models import Tribe, Message, Playlist, Track, Comment, Profile

# Register your models here.

admin.site.register(Tribe)
admin.site.register(Message)
admin.site.register(Playlist)
admin.site.register(Track)
admin.site.register(Comment)
admin.site.register(Profile)
