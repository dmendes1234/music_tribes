from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('login/', views.login_, name='login'),
    path('logout/', views.logout_, name='logout'),
    path('edit_user/', views.edit_user, name='edit_user'),
    path('change_password/', views.change_password, name='change_password'),
    path('create_tribe/', views.create_tribe, name='create_tribe'),
    path('join_tribe/<int:tribe_id>/', views.join_tribe, name='join_tribe'),
    path('tribe_details/<int:tribe_id>/', views.tribe_details, name='tribe_details'),
    path('create_playlist/<int:tribe_id>/', views.create_playlist, name='create_playlist'),
    path('tribe_details/<int:tribe_id>/delete_playlist/<int:playlist_id>/', views.delete_playlist, name='delete_playlist'),
    path('tribe_details/<int:tribe_id>/edit_playlist/<int:playlist_id>/', views.edit_playlist, name='edit_playlist'),
    path('tribe_details/<int:tribe_id>/kick_member/<str:member_name>/', views.kick_member, name='kick_member'),
    path('tribe_details/<int:tribe_id>/leave_tribe/<str:user_name>/', views.leave_tribe, name='leave_tribe'),
    path('tribe_details/<int:tribe_id>/send_message/', views.send_message, name='send_message'),
    path('tribe_details/<int:tribe_id>/delete_message/<int:message_id>/', views.delete_message, name='delete_message'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/', views.playlist_details, name='playlist_details'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/add_track/', views.add_track, name='add_track'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/delete_track/<int:track_id>', views.delete_track, name='delete_track'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/upvote/<int:track_id>/', views.upvote, name='upvote'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/downvote/<int:track_id>/', views.downvote, name='downvote'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/comment/<int:track_id>/', views.comment, name='comment'),
    path('tribe_details/<int:tribe_id>/playlist_details/<int:playlist_id>/delete_comment/<int:comment_id>/', views.delete_comment, name='delete_comment')
] 